#!/usr/bin/env python
# coding=utf-8
#
# Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
#

##############################################################################
# Ink Leaflet - Some simple leaflet making - An Inkscape 1.1+ extension
##############################################################################


import inkex
from inkex import TextElement, Tspan, Group, SvgDocumentElement
from inklinea import Inklin
from slugify import slugify

import sys, uuid, os, time, shutil, datetime, configparser, csv


def read_config_file():
    ink_leaflet_config = configparser.ConfigParser()
    ink_leaflet_config.read('ink_leaflet.ini')

    font_dict = dict(ink_leaflet_config.items('FONT'))

    return font_dict


def create_labels(self, temp_folder, string_list, selection_list, font_dict, parent):
    import time
    """
    Generic Font Families are:
    'serif' (e.g., Times)
    'sans-serif' (e.g., Helvetica)
    'cursive' (e.g., Zapf-Chancery)
    'fantasy' (e.g., Western)
    'monospace' (e.g., Courier)
    """
    bbox_type = self.options.bbox_type_radio

    # text_svg is a returned deepcopy of the svg
    string_list_bbox, returned_text_group, text_svg = Inklin.get_text_string_offset(self, temp_folder, bbox_type,
                                                                                    string_list, font_dict)

    if parent == 'label_svg':
        parent = text_svg

    # Create new group to hold labels:
    label_group = Inklin.create_new_group(self, parent, 'labels', 'layer', 'epoch')

    for text_element, selected in zip(returned_text_group, selection_list):

        text_element.query_all_bbox = string_list_bbox[text_element.get_id()]['query_all_bbox']
        text_offset_x = text_element.query_all_bbox.x1 * -1
        text_offset_y1 = text_element.query_all_bbox.y1 * -1
        text_offset_y2 = text_element.query_all_bbox.y2 * -1
        text_offset_ydiff = text_offset_y2 - text_offset_y1

        selected_width = string_list_bbox[selected.get_id()]['width']
        selected_height = string_list_bbox[selected.get_id()]['height']
        text_width = text_element.query_all_bbox.width
        text_height = text_element.query_all_bbox.height
        x_difference = selected_width - text_width
        y_difference = selected_height - text_height
        selected_y = string_list_bbox[selected.get_id()]['y1']
        selected_x = string_list_bbox[selected.get_id()]['x1']

        if self.options.horizontal_pos_radio == 'left':
            text_element_x = selected_x + text_offset_x
        if self.options.horizontal_pos_radio == 'center':
            text_element_x = selected_x + (x_difference / 2) + text_offset_x
        if self.options.horizontal_pos_radio == 'right':
            text_element_x = selected_x + x_difference + text_offset_x

        if self.options.vertical_pos_radio == 'top':
            text_element_y = selected_y
        if self.options.vertical_pos_radio == 'middle':
            text_element_y = selected_y + (y_difference / 2) + text_offset_y1
        if self.options.vertical_pos_radio == 'bottom':
            text_element_y = selected_y + selected_height

        if self.options.horizontal_pos_radio != 'center':

            if self.options.horizontal_inside_outside_radio == 'inside':
                if self.options.horizontal_pos_radio == 'left':
                    text_element_x = text_element_x
                if self.options.horizontal_pos_radio == 'right':
                    text_element_x = text_element_x

            if self.options.horizontal_inside_outside_radio == 'outside':
                if self.options.horizontal_pos_radio == 'left':
                    text_element_x = text_element_x - text_width
                if self.options.horizontal_pos_radio == 'right':
                    text_element_x = text_element_x + text_width

        if self.options.vertical_pos_radio != 'middle':

            if self.options.vertical_inside_outside_radio == 'inside':
                if self.options.vertical_pos_radio == 'top':
                    text_element_y = text_element_y + text_height + text_offset_y2

                if self.options.vertical_pos_radio == 'bottom':
                    text_element_y = text_element_y + text_height + text_offset_ydiff
                    if self.options.perch_radio != 'baseline':
                        text_element_y = text_element_y + text_offset_y2

            if self.options.vertical_inside_outside_radio == 'outside':
                if self.options.vertical_pos_radio == 'top':
                    text_element_y = text_element_y + text_height + text_offset_ydiff
                    if self.options.perch_radio != 'baseline':
                        text_element_y = text_element_y + text_offset_y2
                if self.options.vertical_pos_radio == 'bottom':
                    text_element_y = text_element_y + text_height + text_offset_y1 + text_offset_ydiff

        text_element.set('x', str(text_element_x / self.cf))
        text_element.set('y', str(text_element_y / self.cf))
        # Remove the text element id to force Inkscape to regenerate id
        # This prevents collisions on 2nd run
        text_element.set('id', '')
        label_group.append(text_element.duplicate())

    parent.append(label_group)
    returned_text_group.delete()
    return text_svg


class InkLeaflet(inkex.EffectExtension):

    def add_arguments(self, pars):
        pars.add_argument("--text_source_radio", type=str, dest="text_source_radio", default='labelling_string')
        pars.add_argument("--bbox_type_radio", type=str, dest="bbox_type_radio", default='visual')
        pars.add_argument("--output_type_radio", type=str, dest="output_type_radio", default='canvas')

        pars.add_argument("--ink_leaflet_notebook", type=str, dest="ink_leaflet_notebook", default=0)

        pars.add_argument("--vertical_pos_radio", type=str, dest="vertical_pos_radio", default='top')
        pars.add_argument("--horizontal_pos_radio", type=str, dest="horizontal_pos_radio", default='left')
        pars.add_argument("--horizontal_inside_outside_radio", type=str, dest="horizontal_inside_outside_radio",
                          default='center')
        pars.add_argument("--vertical_inside_outside_radio", type=str, dest="vertical_inside_outside_radio",
                          default='center')
        pars.add_argument("--perch_radio", type=str, dest="perch_radio", default='right')

        pars.add_argument("--labelling_string", type=str, dest="labelling_string", default='x')

        # pars.add_argument("--font_family_string", type=str, dest="font_family_string", default='x')
        pars.add_argument("--font_size_int", type=float, dest="font_size_int", default=1)
        pars.add_argument("--font_units_combo", type=str, dest="font_units_combo", default='px')
        # Colorpicker for font
        pars.add_argument("--font_fill_cp", type=str, dest="font_fill_cp")

        pars.add_argument("--csv_start_row_cb", type=str, dest="csv_start_row_cb")
        pars.add_argument("--csv_start_row_int", type=float, dest="csv_start_row_int", default=1)
        pars.add_argument("--csv_end_row_cb", type=str, dest="csv_end_row_cb")
        pars.add_argument("--csv_end_row_int", type=float, dest="csv_end_row_int", default=1)

        pars.add_argument("--filename_numbering_prefix_cb", type=str, dest="filename_numbering_prefix_cb")
        pars.add_argument("--csv_prefix_string_cb", type=str, dest="csv_prefix_string_cb")
        pars.add_argument("--csv_prefix_string", type=str, dest="csv_prefix_string", default='x')
        pars.add_argument("--csv_suffix_string_cb", type=str, dest="csv_suffix_string_cb")
        pars.add_argument("--csv_suffix_string", type=str, dest="csv_suffix_string", default='x')

        pars.add_argument("--csv_filename_prefix_cb", type=str, dest="csv_filename_prefix_cb")
        pars.add_argument("--csv_merge_cells_start_int", type=float, dest="csv_merge_cells_start_int", default=1)
        pars.add_argument("--csv_merge_cells_end_int", type=float, dest="csv_merge_cells_end_int", default=1)
        pars.add_argument("--csv_merge_separator_string", type=str, dest="csv_merge_separator_string", default='')

        pars.add_argument("--csv_filepath", type=str, dest="csv_filepath", default=None)

        pars.add_argument("--csv_merge_cells_cb", type=str, dest="csv_merge_cells_cb")
        pars.add_argument("--csv_filename_prefix_cell_int", type=int, dest="csv_filename_prefix_cell_int", default='1')
        pars.add_argument("--base_filename_string", type=str, dest="base_filename_string", default='x')
        pars.add_argument("--export_folder_path", type=str, dest="export_folder_path", default=None)

        pars.add_argument("--stderr_cb", type=str, dest="stderr_cb")
        pars.add_argument("--stdout_cb", type=str, dest="stdout_cb")
        pars.add_argument("--file_write_sleep_value_int", type=int, dest="file_write_sleep_value_int", default='1')

    def effect(self):
        # Sleep Value for Windows 10 file writing
        if Inklin.os_check(self) == 'windows':
            file_write_sleep_value = 5
        else:
            file_write_sleep_value = 0

        # Switch off standard error and standard output
        # To avoid inkex messagebox
        if self.options.stderr_cb == 'true':
            Inklin.set_stderr(self, 'off')
        if self.options.stdout_cb == 'true':
            Inklin.set_stdout(self, 'off')

        found_units = self.svg.unit
        self.cf = Inklin.conversions[found_units]

        temp_folder = Inklin.make_temp_folder(self)

        selection_list = self.svg.selected
        if len(selection_list) < 1:
            inkex.errormsg('Nothing Selected')
            return

        parent = self.svg.get_current_layer()

        # read font settings from the ink_leaflet.ini file
        # Values generated by the gtk3 fontchooser extensioni
        pango_font_dict = read_config_file()

        font_dict = {}

        font_dict['font_family'] = pango_font_dict['family']
        font_dict['font_size'] = self.options.font_size_int
        font_dict['font_units'] = self.options.font_units_combo
        font_dict['font_style'] = pango_font_dict['style']
        font_dict['font_weight'] = pango_font_dict['weight']
        font_dict['font_color'] = Inklin.rgb_long_to_svg_rgb(self, self.options.font_fill_cp)

        csv_filepath = self.options.csv_filepath
        base_filename = slugify(self.options.base_filename_string)
        export_folder_path = self.options.export_folder_path

        if self.options.text_source_radio == 'labelling_string':
            # This is a primitive method which does not allow for commas in the text
            # string_list = self.options.labelling_string.split(',')
            # better to use csv.reader
            labelling_string = self.options.labelling_string
            #inkex.errormsg(labelling_string)
            csv_object = csv.reader([labelling_string])
            # string_list = list(csv_object)[0]
            string_list = list(csv_object)
            # inkex.errormsg(string_list[0])

        else:

            string_list = Inklin.csv_to_string_list(self, csv_filepath)

            if string_list == None:
                inkex.errormsg('Invalid csv file')
                return

            # Let's trim the string list according to csv_start_row and csv_end_row user options
            if self.options.csv_start_row_cb == 'true':
                start_row = int(self.options.csv_start_row_int) - 1
            else:
                start_row = 0
            if self.options.csv_end_row_cb == 'true':
                end_row = int(self.options.csv_end_row_int)
            else:
                end_row = None
            string_list = string_list[start_row:end_row]

            # Lets make sure there no extra spaces in the cells
            for row in string_list:
                for i, item in enumerate(row):
                    row[i] = row[i].strip()

            # Should we prefix or suffix the row ?
            if self.options.csv_prefix_string_cb == 'true':
                for row in string_list:
                    prefix_string = self.options.csv_prefix_string
                    row[0] = prefix_string + row[0]
            if self.options.csv_suffix_string_cb == 'true':
                for row in string_list:
                    row[len(selection_list) - 1] = row[len(selection_list) - 1] + self.options.csv_suffix_string

        if self.options.output_type_radio == 'canvas':
            string_list = string_list[0]

            # Should we merge cells ?
            if self.options.csv_merge_cells_cb == 'true':
                merge_start = int(self.options.csv_merge_cells_start_int) - 1
                merge_end = int(self.options.csv_merge_cells_end_int) - 1
                separator = self.options.csv_merge_separator_string
                string_list[merge_start:merge_end] = [separator.join(string_list[merge_start:merge_end])]
        else:
            # inkex.errormsg(string_list)
            Inklin.check_output_filepath(self, export_folder_path, base_filename)
            parent = 'label_svg'
            row_index = 1
            time_stamp = datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S_")
            for row in string_list:
                error_row = False
                # Should we merge cells ?
                if self.options.csv_merge_cells_cb == 'true':
                    merge_start = int(self.options.csv_merge_cells_start_int) - 1
                    merge_end = int(self.options.csv_merge_cells_end_int) - 1
                    separator = self.options.csv_merge_separator_string
                    row[merge_start:merge_end] = [separator.join(row[merge_start:merge_end])]

                # Should we number the files ?
                if self.options.filename_numbering_prefix_cb == 'true':
                    numbering_prefix = str(row_index).zfill(3) + '_'
                else:
                    numbering_prefix = ''
                # lets get the cell text prefix
                if self.options.csv_filename_prefix_cb == 'true':
                    filename_prefix_cell_text = slugify(
                        row[self.options.csv_filename_prefix_cell_int].replace(prefix_string, '')) + '_'
                else:
                    filename_prefix_cell_text = ''

                for cell in row[1:len(selection_list)]:
                    if cell.strip() == '':
                        inkex.errormsg(f'row {row_index} contains an empty cell - skipping')
                        row_index += 1
                        error_row = True
                        break
                if error_row == True:
                    continue

                label_svg = create_labels(self, temp_folder, row, selection_list, font_dict, parent)
                filename_suffix = time_stamp + str(row_index).zfill(3)
                label_svg_filename = numbering_prefix + filename_prefix_cell_text + base_filename + filename_suffix + '.svg'
                label_svg_filepath = os.path.join(export_folder_path, label_svg_filename)
                label_svg_string = label_svg.root.tostring().decode("utf-8")
                with open(label_svg_filepath, 'w') as svg_file:
                    svg_file.write(label_svg_string)
                row_index += 1
                time.sleep(file_write_sleep_value)


            # Cleanup temp folder
            if hasattr(self, 'inklin_temp_folder'):
                shutil.rmtree(self.inklin_temp_folder)
            return

        create_labels(self, temp_folder, string_list, selection_list, font_dict, parent)
        # Cleanup temp folder
        if hasattr(self, 'inklin_temp_folder'):
            shutil.rmtree(self.inklin_temp_folder)

        # Turn stdout and stderr back on to avoid unclosed /dev/null msg
        # sys.stderr = sys.__stderr__
        # sys.stdout = sys.__stdout__


if __name__ == '__main__':
    InkLeaflet().run()
