<?xml version="1.0" encoding="UTF-8"?>

<!--Copyright (C) [2022] [Matt Cottam], [mpcottam@raincloud.co.uk]-->

<!--        This program is free software; you can redistribute it and/or modify-->
<!--        it under the terms of the GNU General Public License as published by-->
<!--        the Free Software Foundation; either version 2 of the License, or-->
<!--        (at your option) any later version.-->

<!--        This program is distributed in the hope that it will be useful,-->
<!--        but WITHOUT ANY WARRANTY; without even the implied warranty of-->
<!--        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the-->
<!--        GNU General Public License for more details.-->

<!--        You should have received a copy of the GNU General Public License-->
<!--        along with this program; if not, write to the Free Software-->
<!--        Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.-->


<!--        #############################################################################-->
<!--        #  Ink Leaflet - Some simple leaflet making -->
<!--        #  An Inkscape 1.1+ extension -->
<!--        #############################################################################-->

<inkscape-extension xmlns="http://www.inkscape.org/namespace/inkscape/extension">
    <name>Ink Leaflet</name>
    <id>inklinea.ink_leaflet</id>

    <hbox>
        <param name="text_source_radio" type="optiongroup" appearance="radio" gui-description="Should the text be taken from&#10;user text entry or csv file ? " gui-text="Source">
            <option value="labelling_string">Label List</option>
            <option value="csv">CSV</option>
        </param>
        <separator/>
        <param name="bbox_type_radio" type="optiongroup" appearance="radio" gui-description="Align text to the selection visual bounding box (consider paths and filters)&#10;or geometric (do not consider paths and filters)" gui-text="Bbox">
            <option value="visual">Visual</option>
            <option value="geometric">Geometric</option>
        </param>
        <separator/>
        <param name="output_type_radio" type="optiongroup" appearance="radio" gui-description="Output to the Inkscape page, or to files instead" gui-text="Output">
            <option value="canvas">Canvas</option>
            <option value="files">Files</option>
        </param>
    </hbox>

    <param name="ink_leaflet_notebook" type="notebook">

        <page name="alignment_page" gui-text="Alignment">

    <hbox>
    <param name="vertical_pos_radio" type="optiongroup" appearance="radio" gui-text="Vertical">
        <option value="top">Top</option>
        <option value="middle">Middle</option>
        <option value="bottom">Bottom</option>
    </param>

    <param name="horizontal_pos_radio" type="optiongroup" appearance="radio" gui-text="Horizontal">
        <option value="left">Left</option>
        <option value="center">Centre</option>
        <option value="right">Right</option>
    </param>
    </hbox>
    <hbox>
    <param name="horizontal_inside_outside_radio" type="optiongroup" appearance="radio" gui-text="Horiztonal">
        <option value="inside">Inside</option>
        <option value="outside">Outside</option>
    </param>
        <param name="vertical_inside_outside_radio" type="optiongroup" appearance="radio" gui-text="Vertical">
            <option value="inside">Inside</option>
            <option value="outside">Outside</option>
        </param>
    </hbox>
    <hbox>
<param name="perch_radio" type="optiongroup" appearance="radio" gui-description="Should text sit on its baseline ?&#10;(decenders - such as tails on y and j will appear below)&#10;or on its bounding box ?" gui-text="Perch">
<option value="baseline">Baseline</option>
<option value="bbox">Bounding Box</option>
</param>

    </hbox>

<vbox>
    <param name="labelling_string" type="string" gui-description="Basic labl" gui-text="Label List">One,Two,Three,Four,Five,"Elephants,Giraffes",Six,Seven</param>
</vbox>

        </page>

        <page name="font_page" gui-text="Font">

            <label>To set the font size/weight/style</label>
            <label>Please use the Ink Leaflet Font Extension</label>

            <vbox>
                <hbox>
                    <param name="font_size_int" type="float" min="0.1" max="999" gui-text="Font Size">16</param>
                    <param name="font_units_combo" type="optiongroup" appearance="combo" gui-text="">
                        <option value="px">px</option>
                        <option value="pt">pt</option>
                        <option value="mm">mm</option>
                        <option value="cm">cm</option>
                        <option value="in">in</option>
                    </param>
                </hbox>
            </vbox>
            <hbox>
            <param name="font_fill_cp" type="color" appearance="colorbutton" gui-text="Font Colour">0x000000ff</param>
            </hbox>


        </page>

        <page name="csv_settings_page" gui-text="CSV">
            <hbox>
                <param name="csv_start_row_cb" type="boolean" gui-text="" gui-description="">false</param>
                <param name="csv_start_row_int" type="int" min="1" max="9999" gui-text="Start row">1</param>
                <param name="csv_end_row_cb" type="boolean" gui-text="" gui-description="">false</param>
                <param name="csv_end_row_int" type="int" min="1" max="9999" gui-text="End row">2</param>
            </hbox>

            <hbox>
                <param name="csv_prefix_string_cb" type="boolean" gui-text="" gui-description="">false</param>
                <param name="csv_prefix_string" type="string" gui-text="Prefix String">Prefix</param>
            </hbox>

            <hbox>
                <param name="csv_suffix_string_cb" type="boolean" gui-text="" gui-description="">false</param>
                <param name="csv_suffix_string" type="string" gui-text="Suffix String">Suffix</param>
            </hbox>

            <hbox>
                <param name="csv_merge_cells_cb" type="boolean" gui-text="Merge Cells" gui-description="">false</param>
                <param name="csv_merge_cells_start_int" type="int" min="1" max="9999" gui-text="Start">1</param>
                <param name="csv_merge_cells_end_int" type="int" min="1" max="9999" gui-text="End">3</param>
                <param name="csv_merge_separator_string" type="string" gui-text="Separator"> </param>
            </hbox>

        </page>

        <page name="files_page" gui-text="Files">

            <vbox>

                <param type="path" name="csv_filepath" gui-text="CSV" mode="file" filetypes="csv">None Selected</param>
            </vbox>
            <label appearance="header">Naming</label>
            <vbox>
                <param name="base_filename_string" type="string" gui-description="Base for exported filenames" gui-text="Base Filename">svg_output</param>
                <hbox>
                <param name="filename_numbering_prefix_cb" type="boolean" gui-text="Numbering" gui-description="">false</param>

                <param name="csv_filename_prefix_cb" type="boolean" gui-text="" gui-description="">false</param>
                <param name="csv_filename_prefix_cell_int" type="int" gui-description="Prefix filename from csv cell text" gui-text="Csv cell prefix">svg_output</param>
                </hbox>
                <param type="path" name="export_folder_path" gui-text="Export Folder" mode="folder">None Selected</param>

            </vbox>

        </page>

        <page name="about_page" gui-text="About">
            <param name="stderr_cb" type="boolean" gui-text="Suppress stderr" gui-description="">true</param>
            <param name="stdout_cb" type="boolean" gui-text="Supress stdout" gui-description="">true</param>
            <hbox>
            <param name="file_write_sleep_value_int" type="int" gui-description="Workaround for Windows 10 freezing during&#10;batch file export" gui-text="Windows file write delay (seconds)">5</param>
            </hbox>
            <label xml:space="preserve">
▶ Can be used as a simple object labeller...
... or can batch output files from csv field.
▶ No placeholders required.
... It simply works using the bounding box of selected objects,
	mapping fields to their selection order.
▶ Would recommend converting svgs containing embedded images to
	linked images before batching out.(Quicker and Smaller)
		</label>


        </page>




    </param> <!-- Closing tag for noteboox-->

    <effect>
        <object-type>path</object-type>
        <effects-menu>
            <submenu name="Ink Leaflet"/>
        </effects-menu>
    </effect>
    <script>
        <command location="inx" interpreter="python">ink_leaflet.py</command>
    </script>
</inkscape-extension>
