# Ink Leaflet

Ink Leaflet - Some simple leaflet making - An Inkscape 1.1+ extension

## Features

Can be used as a simple object labeller...

.. or can batch output files from csv field.

No placeholders required.

It simply works using the bounding box of selected objects, mapping fields to their selection order.

Would recommend converting svgs containing embedded images to linked images before batching out.

This will save both space and will work quicker
